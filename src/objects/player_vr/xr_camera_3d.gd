extends XRCamera3D

var xr_interface: XRInterface

func _ready():
	var interface = XRServer.find_interface("Native mobile")
	if interface and interface.initialize():
		get_viewport().use_xr = true
