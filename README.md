# Basic-VR-in-Godot-4

Copyright Kris Occhipinti 2023-10-31

(https://filmsbykris.com)

License GPLv3

# Android Key
password is "android"

keytool -genkey -v -keystore debut.keystore -alias android -keyalg RSA -keysize 2048 -validity 10000
